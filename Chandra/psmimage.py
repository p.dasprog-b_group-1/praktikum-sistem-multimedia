from rembg import remove
from PIL import Image

foreground = Image.open('pakdejokowi.jpg')
mask = remove(foreground)
mask.save('pakdejokowi-rbg.png')

background = Image.open('indonesia.jpg')

output = Image.composite(foreground, background, mask)
output.save('hasil.png')