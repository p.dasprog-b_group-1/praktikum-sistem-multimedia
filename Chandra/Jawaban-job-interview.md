# No 1
Demo Image Processing
![imageProcessing](https://gitlab.com/p.dasprog-b_group-1/praktikum-sistem-multimedia/-/raw/main/Chandra/imageProcessing.gif)
# No 2
Youtube Image Processing: https://youtu.be/kdqCZevDAKw

# No 3

Kecerdasan Buatan dapat terlihat dalam kodingan ini melalui penggunaan pustaka "rembg" yang merupakan salah satu teknologi pengenalan objek atau objek removal dengan deep learning.

Pustaka "rembg" digunakan untuk menghapus latar belakang pada gambar "foreground" dengan cara memanfaatkan teknologi Deep Learning untuk mempelajari dan mengenali objek yang terdapat pada gambar. Dalam hal ini, Deep Learning membantu untuk mengenali objek-objek yang ada pada gambar dan memisahkan objek-objek tersebut dari latar belakangnya.

Selain itu, kecerdasan buatan juga terlihat dalam penggunaan pustaka "PIL" (Python Imaging Library) yang membantu untuk memproses dan memanipulasi gambar dengan berbagai teknik seperti cropping, blending, dan compositing.

Dalam penggabungan gambar, kecerdasan buatan turut membantu melalui penggunaan teknik compositing pada fungsi "composite()" yang memungkinkan untuk menggabungkan dua gambar dengan mask yang diberikan. Teknik compositing tersebut memanfaatkan algoritma untuk menggabungkan dua gambar dengan mempertimbangkan opacity dan warna dari masing-masing piksel pada gambar.

Dengan demikian, melalui penggunaan pustaka "rembg" dan "PIL", serta teknik compositing, kodingan ini memanfaatkan kecerdasan buatan untuk memproses gambar dengan lebih efisien dan akurat dalam penghapusan latar belakang dan penggabungan gambar.

# No 4
Demo Audio Processing
![AudioProcessing](https://gitlab.com/p.dasprog-b_group-1/praktikum-sistem-multimedia/-/raw/main/Chandra/AudioProcessing.gif)

# No 5
Youtube Audio Processing : https://youtu.be/RBR-PpKGkak

# No 6
Kecerdasan Buatan dapat terlihat dalam kodingan ini melalui penggunaan pustaka "sounddevice" yang membantu untuk merekam suara dari mikrofon dengan menggunakan teknologi pengolahan sinyal digital.

Selain itu, kecerdasan buatan juga terlihat dalam penggunaan pustaka "scipy.io.wavfile" yang membantu untuk menyimpan file audio dalam format WAV dengan menggunakan teknologi pengolahan sinyal digital.

Dalam hal ini, teknologi pengolahan sinyal digital memanfaatkan algoritma dan teknik-teknik matematika seperti transformasi Fourier, filter FIR/IIR, dan teknik-teknik pemrosesan sinyal digital lainnya untuk memproses dan merepresentasikan sinyal suara dalam bentuk digital. Dalam proses merekam suara, teknologi pengolahan sinyal digital membantu untuk mengambil sampel suara dengan frekuensi yang cukup tinggi sehingga informasi suara yang direkam dapat terekam dengan lebih akurat.

Dengan demikian, melalui penggunaan pustaka "sounddevice" dan "scipy.io.wavfile", serta teknologi pengolahan sinyal digital, kodingan ini memanfaatkan kecerdasan buatan untuk merekam suara dengan lebih efisien dan akurat.
